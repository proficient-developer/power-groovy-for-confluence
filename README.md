# Examples

## Basic Usage

```
import com.atlassian.confluence.spaces.SpaceManager

SpaceManager spaceManager = ComponentLocator.getComponent(SpaceManager.class)
spaceManager.getAllSpaces().each { space -> println(space) }
```

## PluginSettings

```
import com.atlassian.sal.api.pluginsettings.PluginSettings

PluginSettings pluginSettings = pluginSettingsFactory.createSettingsForKey("test_key")
pluginSettings.put("property_name", "value") // set value

return pluginSettings.get("property_name") // get value 
```

## BaseUrl

```
applicationProperties.getBaseUrl()
```

## PluginAccessor

```
pluginAccessor.getEnabledPlugins().each { plugin -> println plugin }
```

## UserAccessor

```
import com.atlassian.confluence.user.UserAccessor

UserAccessor userAccessor = ComponentLocator.getComponent(UserAccessor.class)
def result = ""
userAccessor.getUsers().iterator().forEachRemaining({ result += it.getName() + ", " })

return result
```

## atlassian-confluence.log

```
import com.atlassian.sal.api.ApplicationProperties

ApplicationProperties applicationProperties = ComponentLocator.getComponent(ApplicationProperties.class)
def logFile = new File(applicationProperties.getHomeDirectory().getAbsolutePath() + "/logs/atlassian-confluence.log")
def errorMessage = ""
logFile.eachLine { if (it.contains(" ERROR ")) errorMessage += it + "\n" }

return errorMessage
```